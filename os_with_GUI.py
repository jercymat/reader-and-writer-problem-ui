import tkinter as tk
from PIL import Image, ImageTk
import numpy as np
import threading
import time
import random

# Reader and Writer elements
take_bath = [0,0,0,0,0]
bath_time = [0,0,0,0,0]
clean_time = 0
Queue = []

# GUI elements
usercards = []
usertimecards = []
queuecards = []

# Env and Stat variables
generate_cnt = 0
pop_cnt = 0
START = 0  # 0=暫停 1=開始洗澡 -1=強制退出

# Poisson distribution variables
reader_table = []
writer_table = []
reader_ptr = 0
writer_ptr = 0

# =========GUI=========

def gui_updating_start():
    for a in range(5):
        card = tk.Label(root, text='空位', font=("RiiTegakiFude", 45), bg='#ACD7EA', fg='#495B89')
        usercards.append(card)
        card.place(x=96*a+446, y=220, anchor=tk.CENTER)
    for a in range(5):
        card = tk.Label(root, text='0秒', font=("RiiTegakiFude", 20), bg='#C5C5D3', fg='#AD1842')
        usertimecards.append(card)
        card.place(x=96*a+446, y=291, anchor=tk.CENTER)
    for a in range(10):
        card = tk.Label(root, text='　', font=("RiiTegakiFude", 30), bg='#9B8579', fg='#FFFFFF')
        queuecards.append(card)
        card.place(x=50*a+413, y=468, anchor=tk.CENTER)

    root.update()

    # Launching UI threads
    waiting_queue_thread = threading.Thread(target = update_wating_queue)
    waiting_queue_thread.start()
    yokujou_user_thread = threading.Thread(target = update_yokujou_user)
    yokujou_user_thread.start()

def update_wating_queue():
    global START
    rollText = ['工', '客', '　']
    rollColor = ['#AD1842', '#495B89', '#9B8579']
    nowlength = 0

    while 1:
        if START == 1:
            time.sleep(0.2)
            if START == -1:
                break
            for a in range(10):
                if a < len(Queue):
                    queuecards[a].config(text=rollText[Queue[a]], bg=rollColor[Queue[a]])
                else:
                    queuecards[a].config(text=rollText[2], bg=rollColor[2])

        if START == -1:
                break

    print('update_waiting_queue thread ended.')


def update_yokujou_user():
    global START, clean_time
    rollText = ['空位', '客人', '清掃']
    rollColor = ['#ACD7EA', '#495B89', '#495B89']
    rollFg = ['#495B89', '#FFFFFF', '#AD1842']
    lastStatus = 0
    cleanercard = tk.Label(root, text='清掃工事中', font=("RiiTegakiFude", 45), bg='#AD1842', fg='#FFFFFF')
    cleanertimelabel = tk.Label(root, text='清掃時間：0秒', font=("RiiTegakiFude", 20), bg='#C5C5D3', fg='#1D3557')

    while 1:
        if START == 1:
            time.sleep(0.25)
            if START == -1:
                break
            if lastStatus != 2:
                if take_bath[0] == 2 or take_bath[1] == 2 or take_bath[2] == 2 or take_bath[3] == 2 or take_bath[4] == 2:
                    for cards in usercards:
                        cards.config(fg='#ACD7EA', bg='#ACD7EA')
                    for cards in usertimecards:
                        cards.config(fg='#C5C5D3')
                    cleanercard.place(x=638, y=220, anchor=tk.CENTER)
                    cleanertimelabel.config(text='清掃時間：{}秒'.format(clean_time))
                    cleanertimelabel.place(x=638, y=291, anchor=tk.CENTER)
                else:
                    for a in range(5):
                        usercards[a].config(text=rollText[take_bath[a]], fg=rollFg[take_bath[a]], bg=rollColor[take_bath[a]])
                        usertimecards[a].config(text='{}秒'.format(bath_time[a]))
            else:
                if take_bath[0] == 2:
                    cleanertimelabel.config(text='清掃時間：{}秒'.format(clean_time))
                elif take_bath[0] != 2:
                    cleanercard.place_forget()
                    cleanertimelabel.place_forget()
                    for a in range(5):
                        usercards[a].config(text=rollText[take_bath[a]], fg=rollFg[take_bath[a]], bg=rollColor[take_bath[a]])
                        usertimecards[a].config(text='{}秒'.format(bath_time[a]), fg='#AD1842')
            lastStatus = take_bath[0]


        if START == -1:
            break

    print('update_yokujou_user thread ended.')

# =========Back-end=========

def switchYokujou():
    global START
    if START == 0:
        START = 1
        stl.config(image=stopImage)
    else:
        START = 0
        stl.config(image=goImage)

def exit():
    global START
    START = -1
    root.destroy()


def produce_user():   #隨機產生reader or writer
    global pop_cnt, generate_cnt
    produce_table = np.random.poisson(2.14, 100)
    produce_table = produce_table / 2
    roll_table = np.random.poisson(10, 100)
    produce_ptr = 0
    roll_ptr = 0
    while 1:
        if START == 1:
            generate_cnt += 1
            if roll_ptr >= 100:
                roll_table = np.random.poisson(10, 100)
                roll_ptr = 0
            roll = roll_table[roll_ptr]
            if roll > 10 + roll_table.std() or roll < 10 - roll_table.std():
                roll = 0
            else:
                roll = 1
            roll_ptr += 1
            if len(Queue) < 11 :        #隊伍數量不超過10
                Queue.append(roll)
            else :
                print("Popped!")
                pop_cnt += 1
            if produce_ptr >= 100:
                produce_table = np.random.poisson(2.14, 100)
                produce_table = produce_table / 2
                produce_ptr = 0
            time.sleep(produce_table[produce_ptr])
            produce_ptr += 1

        if START == -1:
            break

    print('produce_user thread ended.')


def Reader_Bath(sit, b_time):      #reader 泡澡流程
    take_bath[sit] = 1
    bath_time[sit] = b_time


    for a in range(b_time):
        time.sleep(1)
        if START == -1:
            break
        bath_time[sit] -= 1

    take_bath[sit] = 0
    
def Writer_Bath(sleep_time):
    global clean_time
    clean_time = sleep_time

    for a in range(5):
        take_bath[a] = 2

    for a in range(sleep_time):
        time.sleep(1)
        if START == -1:
            break
        clean_time -= 1

    for a in range(5):
        take_bath[a] = 0

def poissonRefresh(mode):
    global reader_table, writer_table, reader_ptr, writer_ptr
    if mode == 0:
        reader_table = np.random.poisson(2, 100)
        reader_ptr = 0
    elif mode == 1:
        writer_table = np.random.poisson(2, 100)
        writer_ptr = 0


def mainProcess():
    global START, reader_ptr, writer_ptr
    PU = threading.Thread(target = produce_user)        # User的thread
    W_Bath = threading.Thread(target = Writer_Bath)     # 澡堂員工的thread
    R_Bath = []                                         # 澡堂座位的thread
    for i in range(5):
        R_Bath.append(threading.Thread(target = Reader_Bath, args = (i,)))
    poissonRefresh(0)
    poissonRefresh(1)
    while 1:
        if START == 1:
            if not PU.is_alive():
                PU.start()
            if len(Queue) > 0 and not W_Bath.is_alive():
                if Queue[0] == 1:
                    for i in range(5):
                        if not R_Bath[i].is_alive() and len(Queue) > 0:
                            if Queue[0] == 1:
                                Queue.pop(0)
                                if reader_ptr >= 100:
                                    poissonRefresh(0)
                                R_Bath[i] = threading.Thread(target = Reader_Bath, args = (i, reader_table[reader_ptr],))
                                R_Bath[i].start()
                                reader_ptr += 1
                elif Queue[0] == 0 :
                    if not (W_Bath.is_alive() or R_Bath[0].is_alive() or  R_Bath[1].is_alive() or  R_Bath[2].is_alive() or  R_Bath[3].is_alive() or  R_Bath[4].is_alive() ):
                        Queue.pop(0)
                        if writer_ptr >= 100:
                            poissonRefresh(1)
                        W_Bath = threading.Thread(target = Writer_Bath, args=(writer_table[writer_ptr],))
                        W_Bath.start()
                        writer_ptr += 1

            print("此時此刻浴缸中...")
            print(*take_bath, sep='\t')
            print(*bath_time, sep='s\t', end='')
            print('s')
            print('{}s'.format(clean_time))
            print("在外面排隊的人們")
            print(*Queue, sep=',')
            print()
            time.sleep(0.5)

        if START == -1:
            break
    print('mainProcess thread ended.')

# =========Main Area=========


# Launching main process
yokujou = threading.Thread(target = mainProcess)
yokujou.start()

# Launching GUI
root = tk.Tk()
root.title("澡堂")
root.geometry('960x540')

# Place global GUI elements
bgImg = Image.open('bg.jpg')
bgImg = bgImg.resize((960, 540), Image.ANTIALIAS)
bgImage = ImageTk.PhotoImage(bgImg)
bgl = tk.Label(root, image=bgImage)
bgl.pack()
title = tk.Label(root, text="性福慾場", font=("RiiTegakiFude", 45), bg='#AD1842', fg='#F6F9CE')
title.place(x=160, y=70, anchor=tk.CENTER)
goImg = Image.open('go.jpg')
goImg = goImg.resize((79, 26), Image.ANTIALIAS)
goImage = ImageTk.PhotoImage(goImg)
stl = tk.Button(root, image=goImage, command=switchYokujou, bg='#AD1842', borderwidth=0, highlightthickness=0, padx=0, pady=0)
stl.place(x=187, y=120, anchor=tk.CENTER)
stopImg = Image.open('stop.jpg')
stopImage = ImageTk.PhotoImage(stopImg)
exitImg = Image.open('exit.jpg')
exitImg = exitImg.resize((43, 26), Image.ANTIALIAS)
exitImage = ImageTk.PhotoImage(exitImg)
exl = tk.Button(root, image=exitImage, command=exit, bg='#AD1842', borderwidth=0, highlightthickness=0, padx=0, pady=0)
exl.place(x=111, y=120, anchor=tk.CENTER)

gui_updating_start()
root.mainloop()

# Show statistics when terminating
print('Generated: {}'.format(generate_cnt))
print('Popped: {}'.format(pop_cnt))
if (pop_cnt != 0):
    print('Popping rate: {}%'.format(round(pop_cnt / generate_cnt * 100, 2)))
else:
    print('Popping rate: 0%')
